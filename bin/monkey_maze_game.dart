import 'dart:io';

void main() {
  MainProgram mainProgram = new MainProgram();
  mainProgram.inputDirection();
}

class Obj {
  var _symbol;
  var _x;
  var _y;

  Obj(String symbol, int x, int y) {
    this._symbol = symbol;
    this._x = x;
    this._y = y;
  }

  String getSymbol() {
    return _symbol;
  }

  int getX() {
    return _x;
  }

  int getY() {
    return _y;
  }
}

class Devil extends Obj {
  Devil(int x, int y) : super("D", x, y) {}
}

class Exit extends Obj {
  Exit(int x, int y) : super("E", x, y) {}
}

class Monkey extends Obj {
  Monkey(int x, int y) : super("M", x, y) {}

  void _moveUp() {
    _x--;
  }

  void _moveDown() {
    _x++;
  }

  void _moveLeft() {
    _y--;
  }

  void _moveRight() {
    _y++;
  }
}

class Banana extends Obj {
  var _point;

  Banana(int x, int y, int point) : super("B", x, y) {
    this._point = point;
  }

  int getPoint() {
    return _point;
  }
}

class TableMap {
  var _map = [];
  var _width;
  var _height;
  var _monkey;
  var _devil;
  var _exit;
  var _bananas = [];
  int _point = 0;
  bool _gameOver = false;

  TableMap(int width, int height) {
    this._width = width;
    this._height = height;
    for (int i = 0; i < _height; i++) {
      var row = [];
      for (int j = 0; j < _width; j++) {
        row.add("W");
      }
      _map.add(row);
    }
  }

  void setMonkey(Monkey monkey) {
    this._monkey = monkey;
  }

  void setDevil(Devil devil) {
    this._devil = devil;
  }

  void setExited(Exit exit) {
    this._exit = exit;
  }

  void setWalked(int x, int y) {
    _map[x][y] = "-";
  }

  void addBanana(Banana banana) {
    this._bananas.add(banana);
  }

  void showMap() {
    _map[_monkey.getX()][_monkey.getY()] = _monkey.getSymbol();
    _map[_devil.getX()][_devil.getY()] = _devil.getSymbol();
    _map[_exit.getX()][_exit.getY()] = _exit.getSymbol();

    for (int i = 0; i < _bananas.length; i++) {
      _map[_bananas[i].getX()][_bananas[i].getY()] = _bananas[i].getSymbol();
    }

    for (int i = 0; i < _height; i++) {
      print("");
      stdout.write("|");
      for (int j = 0; j < _width; j++) {
        stdout.write(" ");
        stdout.write(_map[i][j]);
        stdout.write(" |");
      }
      print("");
    }

    print("");
  }

  bool _isDevil(int x, int y) {
    if (_map[x][y] == "D") {
      return true;
    }
    return false;
  }

  bool _isExit(int x, int y) {
    if (_map[x][y] == "E") {
      return true;
    }
    return false;
  }

  bool _isWall(int x, int y) {
    if (_map[x][y] == "W") {
      _showWallMessage();
      return true;
    }
    return false;
  }  

  bool _isBanana(int x, int y) {
    if (_map[x][y] == "B") {
      return true;
    }
    return false;
  }

  void _showPoint() {
    print("Total Point = ${_point}");
  }

  void updatePoint(int point) {
    _point = _point + point;
  }

  void _showWin() {
    breakRange();
    print("You Win!!!");
    _showPoint();
    _gameOver = true;
    breakRange();
  }

  void _showLose() {
    breakRange();
    print("Game Over!");
    print("You Lose!!!");    
    _point = 0;
    _showPoint();
    _gameOver = true;
    breakRange();
  }

  void _showWallMessage() {
    breakRange();
    print("Is Wall, You can't walk! Please input new move direction.");
  }

  bool isCanWalk(String move) {
    if ((move == "W" || move == "w") && (_monkey.getX() - 1) >= 0 && !_isWall(_monkey.getX() - 1, _monkey.getY())) { // MOVE UP
      _map[_monkey.getX()][_monkey.getY()] = "-";
      _monkey._moveUp();
    } else if ((move == "A" || move == "a") && (_monkey.getY() - 1) >= 0 && !_isWall(_monkey.getX(), _monkey.getY() - 1)) {  // MOVE LEFT
      _map[_monkey.getX()][_monkey.getY()] = "-";
      _monkey._moveLeft();
    } else if ((move == "S" || move == "s") && (_monkey.getX() + 1) < _height && !_isWall(_monkey.getX() + 1, _monkey.getY())) { // MOVE DOWN
      _map[_monkey.getX()][_monkey.getY()] = "-";
      _monkey._moveDown();
    } else if ((move == "D" || move == "d") && (_monkey.getY() + 1) < _height && !_isWall(_monkey.getX(), _monkey.getY() + 1)) { // MOVE RIGHT
      _map[_monkey.getX()][_monkey.getY()] = "-";
      _monkey._moveRight();
    }

    if ((move == "W" || move == "w") || (move == "A" || move == "a") || (move == "S" || move == "s") || (move == "D" || move == "d")) {      
      if (_isBanana(_monkey.getX(), _monkey.getY())) {
        //when found B (Banana) >> point + 3
        var banana;
        for (int i = 0; i < _bananas.length; i++) {
          if (_bananas[i].getX() == _monkey.getX() &&
              _bananas[i].getY() == _monkey.getY()) {
            banana = _bananas[i];
          }
        }
        updatePoint(banana.getPoint());
        _bananas.remove(banana);
      } else if (_isExit(_monkey.getX(), _monkey.getY())) {
        //when found E (Exit) >> End Game, show point
        _showWin();
      } else if (_isDevil(_monkey.getX(), _monkey.getY())) {
        //when found D (Devil) >> Game Over, point == 0
        _showLose();
      }
      return true;
    }
    return false;
  }

  bool isEnd() {
    return _gameOver;
  }

  void breakRange() {
    print('------------------------');
  }

  void showWelcome() {
    print('>>> Welcome to Monkey Maze Game <<<');
  }

  void showTitle() {
    print('Map: ');
  }
}

class MainProgram {
  void inputDirection() {
    TableMap map = new TableMap(7, 7); //set map with wall

    //set walk
    map.setWalked(0, 3);
    map.setWalked(1, 1);
    map.setWalked(2, 4);
    map.setWalked(4, 5);
    map.setWalked(5, 2);
    map.setWalked(5, 3);

    //set all banana
    map.addBanana(new Banana(0, 1, 3));
    map.addBanana(new Banana(0, 2, 3));
    map.addBanana(new Banana(0, 4, 3));
    map.addBanana(new Banana(0, 5, 3));
    map.addBanana(new Banana(1, 4, 3));
    map.addBanana(new Banana(2, 1, 3));
    map.addBanana(new Banana(3, 1, 3));
    map.addBanana(new Banana(3, 3, 3));
    map.addBanana(new Banana(3, 4, 3));
    map.addBanana(new Banana(3, 5, 3));
    map.addBanana(new Banana(4, 1, 3));
    map.addBanana(new Banana(4, 3, 3));
    map.addBanana(new Banana(5, 1, 3));
    map.addBanana(new Banana(5, 5, 3));
    map.addBanana(new Banana(6, 5, 3));

    //set Monkey
    map.setMonkey(new Monkey(0, 0));

    //set Devil
    map.setDevil(new Devil(0, 6));

    //set Exit
    map.setExited(new Exit(6, 6));

    // show welcome message
    map.breakRange();
    map.showWelcome();

    while (true) {      
      map.breakRange();
      map.showTitle();
      map.showMap();
      while (true) {
        print("Enter move direction (W,A,S,D): "); //input move direction
        String move = stdin.readLineSync() as String;
        if (map.isCanWalk(move)) {
          break;
        } else {
          map.breakRange();
          print("Monkey can't walk,Please try again.");
          map.breakRange();
        }
      }
      if (map.isEnd()) {
        //End Game
        break;
      }
    }
  }
}
